<?php

namespace Orh\Tapd\Modules;

class Oa extends Base
{
    /**
     * 获取公司所有组织架构.
     *
     * @url https://o.tapd.cn/document/api-doc/API%E6%96%87%E6%A1%A3/TAPD%20API%20%E6%96%87%E6%A1%A3/%E8%8E%B7%E5%8F%96%E7%BB%84%E7%BB%87%E6%9E%B6%E6%9E%84.html
     */
    public function organizations(array $query = []): array
    {
        $uri = 'oa_organizations';
        return $this->http->get($uri, $query);
    }

    /**
     * 获取公司所有组织架构下的成员.
     *
     * @url https://o.tapd.cn/document/api-doc/API%E6%96%87%E6%A1%A3/TAPD%20API%20%E6%96%87%E6%A1%A3/%E8%8E%B7%E5%8F%96%E7%BB%84%E7%BB%87%E6%9E%B6%E6%9E%84%E4%B8%8B%E7%9A%84%E6%88%90%E5%91%98.html
     */
    public function members(array $query = []): array
    {
        $uri = 'oa_organization_members';

        $rules = [
            'organization_id'
        ];
        $this->validate($query, $rules);

        return $this->http->get($uri, $query);
    }
}
